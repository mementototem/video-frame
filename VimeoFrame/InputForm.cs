﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VimeoFrame
{
    public partial class InputForm : Form
    {
        public String VideoUrl { get; set; }

        public InputForm()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.VideoUrl = txtUrl.Text;

            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
