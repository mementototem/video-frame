﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VimeoFrame
{
    public partial class Main : Form
    {
        private const String FAILED = "{{FAILED}}";

        private String vimeo = "http://player.vimeo.com/video/";
        private String youtube = "http://www.youtube.com/embed/";
        private static Regex vimeoMatch = new Regex("^https?://vimeo\\.com/(\\d*)$");
        private static Regex youtubeMatch = new Regex("^https?://(www.)?youtu\\.?be(.com)?/(watch\\?v\\=)?(.*)$");

        public Main()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            browser.AllowNavigation = true;
            browser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(browser_DocumentCompleted);

            using (var inputBox = new InputForm())
            {
                if (inputBox.ShowDialog() == DialogResult.OK)
                {
                    String embedUrl = getEmbedUrl(inputBox.VideoUrl);

                    if (embedUrl.Equals(FAILED))
                    {
                        MessageBox.Show("Wrong URL!");
                    }
                    else
                    {
                        browser.Stop();
                        browser.Navigate(embedUrl);
                    }
                }
                else
                {

                }

                inputBox.Dispose();
            }
        }

        private void browser_Navigating(object sender, WebBrowserNavigatedEventArgs e)
        {
            this.Text = "Navigating";
        }

        private void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            browser = sender as WebBrowser;
            this.Text = "Playing " + e.Url.ToString();
        }

        private String getEmbedUrl(String url)
        {
            Match match = vimeoMatch.Match(url);
            if (match.Success)
            {
                return vimeo + match.Groups[1].Value;
            }

            match = youtubeMatch.Match(url);
            if (match.Success)
            {
                return youtube + match.Groups[4].Value;
            }

            return FAILED;
        }
    }
}
